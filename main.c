#include <ncurses.h>
#include <string.h>
#include "things.h"
#include "morsot.h"
#include "inventory.h"
#include "window.h"
#include <stdlib.h>

//add argc and argv back when needed
int main ( void ) {
    
    WINDOW *main_win;
    WINDOW *inventory_win;
    Character WORLD;
    Character player;
    Character temp;
    Character pointer;

    Monsterl_t * monsters;
    monsters = (Monsterl_t*)malloc(sizeof(Monsterl_t));

    char map[500][500];
    int menu = 0;
    int act = 0;
    int mode = 0;

    //getch() gives ascii code of pressed key to input
    int input=0;

    int mwy, //variables for main window height
        mwx, // width
        mwsy, // height start position
        mwsx; // width start position
    int iwy,  //same shit for inventory window
        iwx,
        iwsy,
        iwsx;

    //initialize ncurses, set modes and initial map load
    init_everything();
    if (!load_map(map)) {
        char maperror[20] = "map1 file not found";
        mvprintw(LINES/2, COLS/2 - strlen(maperror)/2, maperror);
        getch();
        endwin();
        return 0;
    }

    load_monsters(monsters, map, 20);
    player.inv = populate_inventory(&player);
    WORLD.inv = populate_witems(&WORLD);

    //set window values now that the ncurses is loaded
    mwy = (LINES - 4);
    mwx = (COLS - 24);
    mwsy = 2;
    mwsx = 2;

    iwy = (LINES - 4);
    iwx = 16;
    iwsy = 2;
    iwsx = (mwx + 2);

    //player starting coordinates
    player.x = 250;
    player.y = 250;
    player.hp = 100;
    player.damage = 10;
    
    //make ncurses to actually draw base
    refresh();

    //create ncurses windows with given parameters
    main_win = create_newwin(mwy, mwx, mwsy, mwsx);
    inventory_win = create_newwin(iwy, iwx, iwsy, iwsx);   

    //game loop
    do {

        //109 ascii for m, toggles menu option on or off
        if (input == 'm' ){
            menu = (menu+1) % 2;
            curs_set(menu);
        }
        if (input == 'l' || input == 't') {
            act = (act+1) % 2;
            curs_set(act);
            pointer = player;
            mode = (input >> 1) % 2;
        }
        
        if (!menu && !act) {
            //movement, return updated coordinates
            temp = character_move(player, input);
            //check if new position is valid
            if (!collision(&temp, map, monsters))
                player=temp;
            move_monsters(monsters, &player, map);
        }

        //draw map
        draw_map (main_win, mwy, mwx, map, player);
        //draw monsters
        draw_monsters(main_win, mwy, mwx, monsters, player);
        //draw world items
        draw_items(main_win, mwy, mwx, WORLD.inv, player);
        //draw inventory
        draw_inventory(inventory_win, iwy, iwx, player.inv);
        //draw player
        draw_player(main_win, mwy, mwx, player);
        
        if (menu) {
            //navigate inventory
            use_inventory(inventory_win, iwy, input, player.inv);       
        }

        if (act) {
            pointer = character_move(pointer, input);
            action (&pointer, input, mode, monsters, WORLD.inv);
            draw_player(main_win, mwy, mwx, player);
        }

       

        //dirty hp drawing
        move(LINES-2,0);
        clrtoeol();
        mvprintw(LINES-2, 2, "HP: %d", player.hp); //placeholder
        mvprintw(LINES-2, COLS/2-8, "press 0 to exit");

      
        //update things on screen
        refresh();
        refresh_windows(main_win, inventory_win);

        //continue until 0 is pressed (for now)
        //48 is 0 in ascii
    } while((input = getch()) != '0');

    //end ncurses mode
    endwin();

    return 0;
}


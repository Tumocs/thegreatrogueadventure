#ifndef THINGS_H_
#define THINGS_H_

#include "inventory.h"
#include "character.h"
#include "morsot.h"
#include <ncurses.h>

int load_map ( char map[500][500]);

Character character_move(Character player, int input);

int collision(Character * temp, char map[500][500], Monsterl_t * monsters);

void action (Character * pointer, int input, int mode, Monsterl_t * monsters, 
             Inventory_t * WORLD);

#endif // THINGS_H_

#include <stdlib.h>
#include <time.h>
#include "things.h"
#include "morsot.h"

//Fill monster array with crap for now
void load_monsters(Monsterl_t * monsters, char map[500][500], int amount) {
    //seed random
    srand(time(NULL));

    /* Define monster types
    Character troll;
    troll.hp = 100;
    troll.type = 'T';
    troll.colour = 1;

    Character snake;
    snake.hp = 10;
    snake.type = 's';
    snake.colour = 1;

    Character thief;
    thief.hp = 30;
    thief.type = 't';
    thief.colour = 2; */

    //reserve space for monsters
    monsters->amount = amount;
    monsters->mlist = (Character**)malloc(amount * sizeof(Character*));
    for (int i=0; i<amount; i++) {
        monsters->mlist[i] = (Character*)malloc(sizeof(Character));
    }

    //fill array
    for (int i = 0; i < 20; i++) {
        if(i % 3 == 0) {
            monsters->mlist[i]->type = 's';
            monsters->mlist[i]->hp = 10;
            monsters->mlist[i]->colour = 4;
        }
        if(i % 3 == 1) {
            monsters->mlist[i]->type = 't';
            monsters->mlist[i]->hp = 30;
            monsters->mlist[i]->colour = 5;
        }
        if(i % 3 == 2) {
            monsters->mlist[i]->type = 'T';
            monsters->mlist[i]->hp = 100;
            monsters->mlist[i]->colour = 6;
        }
        //try random coordinates and reroll if on blocked terrain
        do {
            monsters->mlist[i]->x = rand() % 499;
            monsters->mlist[i]->y = rand() % 499;
        }while(collision (monsters->mlist[i], map, monsters));
    }

}

//super simple monster movement
void move_monsters(Monsterl_t * monsters, Character * player, char map[500][500]) {
    int x;
    int y;
    Character temp;
    //loop through monster array
    for (int i = 0; i < 20; i++) {
        temp.x = monsters->mlist[i]->x;
        temp.y = monsters->mlist[i]->y;
        //get distance
        x = temp.x - player->x;
        y = temp.y - player->y;
        //check proximity and move
        if (abs(x) < 8 && abs(y) < 8) {
            if(!ycloser(x,y)) {
                if (y < 0)
                    temp.y++;
                if (y > 0)
                    temp.y--;
            }
            else if (ycloser(x,y)) {
                if (x < 0)
                    temp.x++;
                if (x > 0)
                    temp.x--;
            }
        }
        //check collision with terrain and player
        if (!collision(&temp, map, monsters) && !hit_player(temp, player)){
                monsters->mlist[i]->x=temp.x;
                monsters->mlist[i]->y=temp.y;
        }
    }
    return;
}

//function to see if monster is closer on x or y axis
//return 1 is y is closer 0 if x
int ycloser(int x, int y) {
    if ((abs(x)-abs(y)) > 0)
        return 1;
    return 0;
}

//check if monster would be on player, reduce hp and return 1 if it would be
int hit_player (Character monster, Character * player) {
    if (monster.x == player->x && monster.y == player->y) {
        player->hp -= 10;
        return 1;
    }
    return 0;
}



#ifndef _WINDOW_H
#define _WINDOW_H
#include <ncurses.h>
#include "character.h"
#include "inventory.h"
#include "morsot.h"

WINDOW * create_newwin(int height, int width, int starty, int startx);
void init_everything ( void );
void refresh_windows (WINDOW * window, WINDOW * window2);
void draw_map ( WINDOW * window, int wy, int wx, char map[500][500], 
                Character player);
void draw_player ( WINDOW * window, int wy, int wx, Character player );
void draw_inventory ( WINDOW * window, int wy, int wx, 
                      Inventory_t * inventory);
void use_inventory ( WINDOW * window, int wy, int input, Inventory_t * inventory);
void draw_monsters(WINDOW * window, int wx, int wy, 
                   Monsterl_t *monsters, Character player);
void draw_items(WINDOW * window, int wy, int wx,
        Inventory_t * inventory, Character player);
#endif // _WINDOW_H

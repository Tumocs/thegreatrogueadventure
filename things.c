#include <ncurses.h>
#include "things.h"

//load binary mapfile, requires map pointer
int load_map (char map[500][500]) {
    FILE * mapfile;
    if((mapfile = fopen ("map1", "r")) == NULL) {
        return 0;
    }
    fread(map, 500*500*sizeof(char), 1, mapfile);
    fclose(mapfile);
    return 1;
}


//initial input handling
Character character_move (Character player, int input) {
    switch (input) {
        case KEY_LEFT: 
        case 'a': 
            if (player.x > 1)
                player.x--;
            break;
        case KEY_RIGHT: 
        case 'd': 
            if (player.x < 498)
                player.x++;
            break;
        case  KEY_UP:
        case 'w':
            if (player.y > 1)    
                player.y--;
            break;
        case KEY_DOWN:
        case 's': 
            if(player.y < 498)
                player.y++;
            break;
        case 'q':
        case KEY_A1:
           if (player.x > 1 && player.y > 1) {
                player.x--;
                player.y--;
            }
            break;
        case 'e':
        case KEY_A3:
            if(player.x < 498 && player.y > 1){
                player.x++;
                player.y--;
            }
            break;
        case 'z':
        case KEY_C1: 
            if(player.x > 1 && player.y < 498){
                player.x--;
                player.y++;
            }
            break;
        case 'x':
        case KEY_C3:
           if (player.x <498 && player.y <498) {
                player.x++;
                player.y++;
            }
            break;

    }
    return player;
}

//initial super simple collision detection
//return 0 if new position is accessible
int collision (Character *  temp, char map[500][500], Monsterl_t * monsters) {
    
    char blocker[2] = {'^', '~'};
    for (int i = 0; i < 2; i++) {
        if (map[temp->y][temp->x] == blocker[i])
            return 1;
    }

    //TODO dynamic amount of monsters, this is ugly
    for (int i = 0; i <monsters->amount; i++) {
        if (monsters->mlist[i]->x == temp->x && monsters->mlist[i]->y == 
            temp->y && monsters->mlist[i] != temp) {
            monsters->mlist[i]->hp -= temp->damage;
            return 1;
        }
    }

    return 0;
}

void action (Character * pointer, int input, int mode, Monsterl_t * monsters, 
             Inventory_t * WORLD) {

    //consider refactoring sooner or later, this start to be a
    //tangled mess
    
    if (input == KEY_ENTER) {
        switch (mode) {
            case 0:
                //implement look
                break;
            case 1:
                //implement pick up
                break;
            default:
                break;
        }

    }

}



CC = gcc
CFLAGS = -Wall -Wextra -Wpedantic

supergame : main.o things.o morsot.o inventory.o window.o
	$(CC) -o supergame main.o window.o things.o morsot.o inventory.o -lncurses

main.o : main.c

window.o : window.c

things.o : things.c

morsot.o : morsot.c

inventory.o : inventory.c


clean:
	rm -f mapcheck maptobin supergame main.o things.o morsot.o inventory.o window.o

maptools:
	$(CC) -o mapcheck mapcheck.c
	$(CC) -o maptobin maptobin.c

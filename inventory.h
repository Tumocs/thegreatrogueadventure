#ifndef INVENTORY_H_
#define INVENTORY_H_

#include "character.h"
typedef struct inventory Inventory_t;

typedef struct item {
    char name[20];
    char icon;
    int xy[2];
    int slot; //0 lhand; 1 rhand; 2 torso; 3 head; 4 legs; 5 misc;
    int wearable; //0 no; 1 yes;
    int useable; //0 no; 1 yes;
    Character * owner; //point to owner structure, NULL for not owned
} Item_t;

struct inventory {
   Item_t ** items;
   int space;
   int size;
   int menupointer;
};

Inventory_t * populate_witems ( Character * owner );
Inventory_t * populate_inventory ( Character * owner );
void pick_up (Character * owner, Inventory_t * inventory, Item_t * item);


#endif //INVENTORY_H_

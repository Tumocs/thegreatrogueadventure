#ifndef _CHARACTER_H
#define _CHARACTER_H

typedef struct {
    int x;
    int y;
    int hp;
    char type;
    struct inventory * inv;
    int damage;
    int colour;
} Character;

#endif // _CHARACTER_H

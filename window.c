#include "window.h"

//Function to create new window with outlines
WINDOW * create_newwin (int height, int width, int starty, int startx) {
    WINDOW * local_win;
    //create new window with given parameters
    local_win = newwin(height, width, starty, startx);
    //draw a box around it
    box(local_win, 0, 0);
    //refresh it to make it appear
    wrefresh(local_win);
    return local_win;
}

//group ncurses initialization functions to keep main cleaner
void init_everything ( void ) {

    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    start_color();
    curs_set(0);

    init_color(COLOR_MAGENTA, 255, 255, 255);
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_CYAN, COLOR_BLUE);
    init_pair(3, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    init_pair(5, COLOR_WHITE, COLOR_BLACK);
    init_pair(6, COLOR_RED, COLOR_BLACK);

}

//refresh subwindows here to keep main cleaner
void refresh_windows (WINDOW * w1, WINDOW * w2) {
    wrefresh(w1);
    wrefresh(w2);
}

//loop to draw map based on the player coordinates
void draw_map (WINDOW * window, int wy, int wx, char map[500][500], Character player) {
    int cx, cy;
    int color;
    //set coordinates of the middle of the window to player values
    cx = player.x;
    cy = player.y;
    //check if they are within reasonable boundaries
    if (cy < wy/2)
        cy = wy/2;
    else if (cy > (500-wy/2))
        cy = 500-wy/2;
    if (cx < wx/2)
        cx = wx/2;
    else if (cx > (500-wx/2))
        cx = 500-wx/2;

    //temporary coordinate printing
    move(0,0);
    clrtoeol();
    mvprintw(0, 0, "x:%d y:%d", cx, cy);

    for(int i=1; i<wy-1; i++) {
        for(int j=1; j<wx-1; j++) {
            //set colour according to icon
            if (map[cy-(wy/2)+i][cx-(wx/2)+j] == '^')
                color = 3;
            if (map[cy-(wy/2)+i][cx-(wx/2)+j] == '.')
                color = 1;
            if (map[cy-(wy/2)+i][cx-(wx/2)+j] == '~')
                color = 2;
            if (map[cy-(wy/2)+i][cx-(wx/2)+j] == ',')
                color = 4;

            //colour on
            wattron(window, COLOR_PAIR(color));
            //draw tile, from map array, according to set coordinates
            //-half of the width of window all the way to the other end
            mvwaddch(window, i, j, map[cy-(wy/2)+i][cx-(wx/2)+j]);
            //colour off
            wattroff(window, COLOR_PAIR(color));
        }
    }
} 

//determine the correct player position on screen
//takes target window, width and height of it and player struct
//as arguments
void draw_player (WINDOW * window, int wy, int wx, Character player) {
    int posx, posy;
    //position in the middle until player closes the edges
    posx = wx/2;
    posy = wy/2;
    if (player.x < wx/2)
        posx = player.x;
    if (player.x > (500-wx/2))
        posx = wx-(500-player.x);
    if (player.y < wy/2)
        posy = player.y;
    if (player.y > (500-wy/2))
        posy = wy-(500-player.y);
    //temporary position indicator for debugging
    move(1,0);
    clrtoeol();
    mvprintw(1, 0, "px:%d py:%d", posx, posy);

    //draw character to position as bolded @
    wmove(window, posy, posx);
    waddch(window, '@' | A_BOLD);

}

//function to draw inventory contents
void draw_inventory (WINDOW * window, int wy, int wx, Inventory_t * inventory) {    

    //TODO only draw amount of rows that fit to screen
    //remember inventory position
    //up and down arrows if not at top or bottom

    for(int i = 0; i < inventory->size-inventory->space; i++) {
        mvwaddstr(window, i+1, 2, inventory->items[i]->name);
    }

}

//initial inventory movement
void use_inventory ( WINDOW * window, int wy, int input, Inventory_t * inventory) {
    
    //TODO scrolling menu

    switch (input) {
        case KEY_UP:
        case 'w':
            inventory->menupointer--;
            break;
        case KEY_DOWN:
        case 's':
            inventory->menupointer++;
            break;
        default:
            break;
    }

    if (inventory->menupointer < 1)
        inventory->menupointer = inventory->size-inventory->space;
    if (inventory->menupointer > inventory->size-inventory->space)
        inventory->menupointer = 1;


    wmove(window, inventory->menupointer, 1);

    return;
}

//draw the monsters on map
void draw_monsters(WINDOW * window, int wy, int wx,
        Monsterl_t *monsters, Character player) {
    int winminx, winminy, winmaxx, winmaxy;
    //define window borders in relation to player coordinates
    winminx = player.x - wx/2;
    winminy = player.y - wy/2;

    if (winminx < 0)
        winminx = 0;
    if (winminy < 0)
        winminy = 0;

    if (winminx > 500-wx)
        winminx = 500-wx;
    if (winminy > 500-wy)
        winminy = 500-wy;

    winmaxx = winminx + wx;
    winmaxy = winminy + wy;

    //draw the monsters if their coordinates are inside window border
    for (int i = 0; i < monsters->amount; i++) {
        if(monsters->mlist[i]->x > winminx && monsters->mlist[i]->x < winmaxx-1 
           && monsters->mlist[i]->y > winminy && 
           monsters->mlist[i]->y < winmaxy-1) {
            wattron(window, COLOR_PAIR(monsters->mlist[i]->colour));
            mvwaddch(window, monsters->mlist[i]->y-winminy, 
                     monsters->mlist[i]->x-winminx, monsters->mlist[i]->type);
            wattroff(window, COLOR_PAIR(monsters->mlist[i]->colour));
        }
    }
}

/* Draw world items on map, take main window and size of it,
 * Inventory pointer of world and player for relative coordinates */
void draw_items(WINDOW * window, int wy, int wx,
                Inventory_t * inventory, Character player) {
    int winminx, winminy, winmaxx, winmaxy;
    int entries = inventory->size-inventory->space;
    //define window borders in relation to player coordinates
    winminx = player.x - wx/2;
    winminy = player.y - wy/2;

    if (winminx < 0)
        winminx = 0;
    if (winminy < 0)
        winminy = 0;

    if (winminx > 500-wx)
        winminx = 500-wx;
    if (winminy > 500-wy)
        winminy = 500-wy;

    winmaxx = winminx + wx;
    winmaxy = winminy + wy;

    //draw the items if their coordinates are inside window border
    for (int i = 0; i < entries; i++) {
        if(inventory->items[i]->xy[0] > winminx && 
           inventory->items[i]->xy[0] < winmaxx-1 &&
           inventory->items[i]->xy[1] > winminy &&
           inventory->items[i]->xy[1] < winmaxy-1) {
            mvwaddch(window, inventory->items[i]->xy[1]-winminy, 
                     inventory->items[i]->xy[0]-winminx,
                     inventory->items[i]->icon);
        }
    }
}


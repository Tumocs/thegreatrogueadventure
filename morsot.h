#ifndef MORSOT_H_
#define MORSOT_H_
#include "character.h"

typedef struct {
    Character ** mlist;
    int amount;
} Monsterl_t;

void load_monsters(Monsterl_t *monsters, char map[500][500], int amount);
void move_monsters(Monsterl_t * monsters, Character * player, char map[500][500]);
int ycloser(int x, int y);
int hit_player (Character monster, Character * player);
#endif // MORSOT_H_

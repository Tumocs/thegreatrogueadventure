#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    FILE * binfile;
    char map[500][500];

    if (argc != 2) {
        puts("USAGE");
        puts("mapcheck <mapfile>");
        return 0;
    }

    binfile = fopen(argv[1], "r");
    fread(map, sizeof(map), 1, binfile);

    for(int i = 0; i<500; i++){
        for(int j = 0; j < 500; j++)
            putchar(map[i][j]);
        putchar('\n');
    }

    fclose(binfile);

    return 0;
}

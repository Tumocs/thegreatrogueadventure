#include "inventory.h"
#include <stdlib.h>
#include <string.h>

Inventory_t * populate_witems ( Character * owner ) {

    Inventory_t * inventory;
    inventory = (Inventory_t*)malloc(sizeof(Inventory_t));
    inventory->space = 100;
    inventory->size = 100;

    inventory->items = (Item_t**)malloc(sizeof(Item_t*));

    Item_t * bow;
    bow = (Item_t*)malloc(sizeof(Item_t));
    strcpy(bow->name, "bow");
    bow->icon = '}';
    bow->xy[0] = 245;
    bow->xy[1] = 245;
    bow->wearable = 1;
    bow->useable = 0;

    pick_up(owner, inventory, bow);
    
    return inventory;
}

Inventory_t * populate_inventory ( Character * owner ) {

    Inventory_t * inventory;
    inventory = (Inventory_t*)malloc(sizeof(Inventory_t));
    inventory->space = 20;
    inventory->size = 20;
    inventory->menupointer = 1;
    inventory->items = (Item_t**)malloc(sizeof(Item_t*));

    Item_t * sword;
    sword = (Item_t*)malloc(sizeof(Item_t));
    strcpy(sword->name, "sword");
    sword->wearable = 1;
    sword->useable = 0;

    Item_t * ass;
    ass = (Item_t*)malloc(sizeof(Item_t));
    strcpy(ass->name, "ass");
    ass->wearable = 1;
    ass->useable = 0;
    
    Item_t * stone;
    stone = (Item_t*)malloc(sizeof(Item_t));
    strcpy(stone->name, "stone");
    stone->useable = 1;
    stone->wearable = 0;

    pick_up(owner, inventory, sword);
    pick_up(owner, inventory, ass);
    pick_up(owner, inventory, stone);

    return inventory;
}

void pick_up (Character * owner, Inventory_t * inventory, Item_t * item) {
    if (inventory->space > 0) {
        //item.index = 20-inventory->space; //old shit
        inventory->items[inventory->size-inventory->space] = item;
        item->owner = owner;
        inventory->space--;
    }
    
    return;
}

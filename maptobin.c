#include <stdio.h>
#include <stdlib.h>

int main ( int argc, char *argv[] ) {
    FILE * textfile;
    FILE * mapfile;

    char binname[20];
    char map[500][500];

    if (argc != 2) {
        puts("USAGE:");
        puts("maptobin <textfile>");
        return 0;
    }

    textfile = fopen(argv[1], "r");
    for (int i = 0; i < 500; i++) {
        fscanf(textfile, "%s", map[i]);
    }

    fclose(textfile);
    
    for (int i = 0; i<500; i++) {
        for (int j = 0; j<500; j++){
            if (map[i][j] == '\0' || map[i][j] == '\n' || map[i][j] == '\t'
                    || map[i][j] == ' ')
                map[i][j] = '.';
        }
    }

    puts("Give name of output file");
    scanf("%s", binname);
    mapfile = fopen (binname, "w");

    fwrite(map, sizeof(map), 1, mapfile);
    fclose(mapfile);

    puts("DONE");

    return 0;

}
